# Example Drupal 8

Example Docker stack with Drupal 8 managed by Composer

## How to start stack

1. [Install Docker](https://www.docker.com/community-edition)
2. Clone this GIT repository
3. Inside directory run: `docker-compose up -d`
4. For the first time use a Composer: `docker exec -itu www-data drupalcomposermemcached_web_1 composer install`

After a while you can:

### Open app in the broser

http://localhost:8027

### Open shell into container

```
docker exec -itu www-data drupalcomposermemcached_web_1 bash
```

Inside container you can:

- List Drupal commands: `drupal list`
- Download module: `composer require drupal/devel`
- Enable module: `drupal moi devel`
