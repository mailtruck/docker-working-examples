# Requirements

- GitLab (inc. hosted gitlab.com)
- [Rancher](http://rancher.com/rancher/) with development and production environments (orchestration Cattle or soon Rancher 2)
- [Rancher Active Proxy](https://github.com/adi90x/rancher-active-proxy) deployed in environments

# How to configure GitLab and Rancher

1. In Rancher create API keys for development and production environment.

2. In GitLab project open CI settings and configure this secret variables:

    | Variable name                 | Description                   | Example value
    | ------------------------------|-------------------------------|--------------------------------------------
    | REVIEW_DOMAIN                 | domain for [review apps](https://docs.gitlab.com/ee/ci/review_apps/) | ci.example.com
    | RANCHER_URL                   | API endpoint with environment | https://rancher.example.com/v1/projects/1a5
    | RANCHER_ACCESS_KEY            |                               | 719BCB705C8EBC19A82B
    | RANCHER_SECRET_KEY            |                               | XM6JpcjpgdS7Tqt5eHbzNo4487YSus69Q7vALFDB
    | PRODUCTION_DOMAIN             | ↓ the same for production ↓   | www.example.com
    | RANCHER_URL_PRODUCTION        |                               | https://rancher.example.com/v1/projects/1a2
    | RANCHER_ACCESS_KEY_PRODUCTION |                               | 7D89B37A2DF34D4218AA
    | RANCHER_SECRET_KEY_PRODUCTION |                               | LRcYBLAxyieXiaef9apCkkQnCQBHrWodTFHEJP4w

3. Put init database with demo data to the volume my-project-db-demo-data. If you do not need to initialize database by default schema and data, just remove volume in docker-compose.ci.yml.

4. Add GitLab's private registry creditials ([Access Tokens in your profile](https://gitlab.com/profile/personal_access_tokens), read_registry permision is enough) to Rancher.

# How to use it on local

[See README.md in drupal-composer-memcached](../drupal-composer-memcached/README.md)
