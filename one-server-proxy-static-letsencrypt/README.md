# How to configure stack

1. Configure your domains in `docker-compose.yml`
2. Configure domains with HTTPS and what will be on static hosting in `.env`
3. [Create access token in your GitLab profile](https://gitlab.com/profile/personal_access_tokens) and put it in `secrets.env`

# Checklist for new static site

1. Create webhook in project settings -> integrations:
   - URL: https://ci-deployer.example.com/deployer
   - Check Build events
   - Generate token for GitLab to authenticate to deployer, then fill it into Secret token field and `secrets.env`
   - Enable SSL verification

2. Configure `.gitlab-ci.yml` to generate artifacts with a static site (e.g. use templates in GitLab web interface)

3. Create symlink from CI hostname for master branch to production domain in /sites in deployer container:
    ```
    ln -s master-examplecom.ci.example.com www.example.com
    ```
