#!/bin/sh

echo "Waiting for database to be available..."


until drush sql-query "SELECT 1" &> /dev/null
do
	sleep 1
done
